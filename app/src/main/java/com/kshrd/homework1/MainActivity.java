package com.kshrd.homework1;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {

    private ImageView androidN, androidO;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        setTitle("Android Version");
        androidN = findViewById(R.id.androidN);
        androidO = findViewById(R.id.androidO);
        androidN.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAndroidN();
            }
        });
        androidO.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                gotoAndroidO();
            }
        });
    }

    private void gotoAndroidN(){
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("image", R.drawable.image1);
        intent.putExtra("text", R.string.androidN);
        startActivityForResult(intent, 1);
    }

    private void gotoAndroidO(){
        Intent intent = new Intent(this, SecondActivity.class);
        intent.putExtra("image", R.drawable.image2);
        intent.putExtra("text", R.string.androidO);
//        startActivity(intent);
        startActivityForResult(intent, 1);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if(requestCode == 1){
            if(resultCode == RESULT_OK){
                Toast.makeText(MainActivity.this, data.getStringExtra("inputtext"), Toast.LENGTH_SHORT).show();
            }
        }
    }
}