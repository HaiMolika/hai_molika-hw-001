package com.kshrd.homework1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class SecondActivity extends AppCompatActivity {

    private ImageView image;
    private TextView text;
    private EditText msg;
    private Button btnsend;
    private Intent intent;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);
        setTitle("Information");
//         defined component id
        image = findViewById(R.id.image);
        text = findViewById(R.id.description);
        btnsend = findViewById(R.id.btnsend);
        msg = findViewById(R.id.txtsend);

        intent = getIntent();
        image.setImageResource(intent.getIntExtra("image", 0));
        text.setText(intent.getIntExtra("text", 0));

        btnsend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                intent.putExtra("inputtext", msg.getText().toString());
                Log.e("input text", msg.getText().toString());
                setResult(RESULT_OK, intent);
                finish();
            }
        });
    }
}